//
//  AppDelegate.swift
//  LineTool
//
//  Created by kasim on 2019/1/31.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import LineSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // swiftlint:disable line_length
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

//        LoginManager.shared.setup(channelID: "1644164886", universalLinkURL: nil)
//
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        if LoginManager.shared.isAuthorized {
//            self.window?.rootViewController = storyboard.instantiateViewController(withIdentifier: "CustomTabBarController")
//        } else {
//            self.window?.rootViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
//        }

        let storyboard = UIStoryboard(name: "Seller", bundle: nil)
        self.window?.rootViewController = storyboard.instantiateViewController(withIdentifier: "SellerNavigationController")

        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        return LoginManager.shared.application(app, open: url, options: options)
    }

    func application(
        _ application: UIApplication,
        continue userActivity: NSUserActivity,
        restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        return LoginManager.shared.application(application, open: userActivity.webpageURL)
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    // swiftlint:enable line_length
}
