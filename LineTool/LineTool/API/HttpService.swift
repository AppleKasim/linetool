//
//  HttpService.swift
//  LineTool
//
//  Created by kasim on 2019/2/14.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import RxSwift
import Moya

public struct HttpService {
    let sellerProvider = MoyaProvider<SellerApiConfig>()
    let buyerProvider = MoyaProvider<BuyerApiConfig>()

    func request<Element: Codable>(config: TargetType) -> Observable<Result<Element>> {
        return Observable.create({ (observable) -> Disposable in
            let primitiveSequence: PrimitiveSequence<SingleTrait, Response>

            switch config {
            case let seller as SellerApiConfig:
                primitiveSequence = self.sellerProvider.rx.request(seller)
            case let buyer as BuyerApiConfig:
                primitiveSequence = self.buyerProvider.rx.request(buyer)
            default:
                fatalError("錯誤的Api Config")
            }

            let callBack = primitiveSequence
                .subscribe { (responseResult) in
                    switch responseResult {
                    case let .success(response):
                        do {
                            //                            //顯示response結果
                            //                            let responseString = String.init(data: response.data, encoding: String.Encoding.utf8)!
                            //
                            //                            print("responseString: \(responseString.replacingOccurrences(of: "\\/", with: "/").replacingOccurrences(of: "\"", with: "\""))")

                            let decoder = JSONDecoder()
                            let data = try decoder.decode(ReponseData<Element>.self, from: response.data)
                            let subjects = data.data
                            let result = (subjects == nil) ? Result.empty: Result.succeed(message: data.message!, data: subjects!)
                            observable.onNext(result)
                        } catch let error {
                            self.requestError(message: error.localizedDescription)
                            observable.onNext(Result.failed(message: error.localizedDescription))
                        }
                    case let .error(error):
                        self.requestError(message: error.localizedDescription)
                        observable.onNext(Result.failed(message: error.localizedDescription))
                    }
            }

            return callBack
        })
    }

    private func requestError(message: String) {
        // 统一处理错误
    }

    func test(imageData: [Data], parameters: [String: Any]) -> Observable<Result<[GoodsResponse]>> {
        return HttpService().request(config: SellerApiConfig.goodsUpload(imageData: imageData, urlParameters: parameters))
    }
}
