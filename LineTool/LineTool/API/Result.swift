//
//  Result.swift
//  LineTool
//
//  Created by kasim on 2019/2/14.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum Result<T: Codable> {
    case empty
    case succeed(message: String, data: T)
    case failed(message: String)
}

extension Result {
    var isValid: Bool {
        switch self {
        case .succeed:
            return true
        default:
            return false
        }
    }
}

extension Result {
    var textColor: UIColor {
        switch self {
        case .succeed:
            return UIColor(red: 138.0 / 255.0, green: 221.0 / 255.0, blue: 109.0 / 255.0, alpha: 1.0)
        case .empty:
            return UIColor.black
        case .failed:
            return UIColor.red
        }
    }
}

extension Result {
    var description: String {
        switch self {
        case let .succeed(message, _):
            return message
        case .empty:
            return ""
        case let .failed(message):
            return message
        }
    }
}
