//
//  BuyerApiConfig.swift
//  LineTool
//
//  Created by kasim on 2019/2/14.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import Moya

public enum BuyerApiConfig {
    case productType()
    case categories()

}

extension BuyerApiConfig: TargetType {
    public var baseURL: URL {
        return URL(string: Global.baseUrl)!
    }

    public var path: String {
        switch self {
        case .productType:
            return "/album/product_type"
        case .categories:
            return "/album/categories"
        }
    }

    public var method: Moya.Method {
        switch self {
            //        case .report:
        //            return .post
        default:
            return .get
        }
    }

    public var task: Task {
        //        var parameters: [String: Any] = [:]
        switch self {
            //        case .rows(let categoryId, let page):
            //            parameters = ["category_id": categoryId, "page": page]
            //        case .userAlbums(let creatorId, let page):
            //            parameters = ["creator_id": creatorId, "page": page]
            //        case .info(let albumId):
            //            parameters = ["album_id": albumId]
            //        case let .voices(albumId, page):
            //            parameters = ["album_id": albumId, "page": page]
            //        case let .albumHots(page):
            //            parameters = ["page": page]
            //        case let .report(voiceId, content):
        //            parameters = ["album_id": voiceId, "content": content]
        default:
            return .requestPlain
        }
        //        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }

    //是否执行Alamofire验证
    public var validate: Bool {
        return false
    }

    //这个就是做单元测试模拟的数据，只会在单元测试文件中有作用
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }

    public var headers: [String: String]? {
        return nil
    }
}
