//
//  ReponseData.swift
//  LineTool
//
//  Created by kasim on 2019/2/14.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation

class ReponseData<T: Codable>: Codable {
    var status: String?
    var message: String?
    var path: String?
    var data: T?
}
