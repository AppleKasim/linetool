//
//  SellerApiConfig.swift
//  LineTool
//
//  Created by kasim on 2019/2/14.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import Moya

public enum SellerApiConfig {
    case goodsUpload(imageData: [Data], urlParameters: [String: Any])
}

extension SellerApiConfig: TargetType {
    public var baseURL: URL {
        return URL(string: Global.baseUrl)!
    }

    public var path: String {
        switch self {
        case .goodsUpload:
            return "/test"
        }
    }

    public var method: Moya.Method {
        switch self {
        case .goodsUpload:
            return .post
        }
    }

    public var task: Task {
//        var parameters: [String: Any] = [:]
        switch self {
        case let .goodsUpload(imageData, urlParameters):

            var multipartFormData: [MultipartFormData] = []
            for (index, data) in imageData.enumerated() {
                let formData = MultipartFormData(provider: .data(data), name: "name\(index)",
                                                  fileName: "file_name\(index)", mimeType: "image/jpg")
                multipartFormData.append(formData)
            }

            return .uploadCompositeMultipart(multipartFormData, urlParameters: urlParameters)

//        case .rows(let categoryId, let page):
//            parameters = ["category_id": categoryId, "page": page]

        }
//        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }

    //是否执行Alamofire验证
    public var validate: Bool {
        return false
    }

    //这个就是做单元测试模拟的数据，只会在单元测试文件中有作用
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }

    public var headers: [String: String]? {
        return ["enctype": "multipart/form-data", "Content-Type": "application/json"]
    }
}
