//
//  ValidationResult.swift
//  LineTool
//
//  Created by kasim on 2019/2/17.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum ValidationResult {
    case initial
    case succeed(message: String)
    case empty(message: String)
    case failed(message: String)
}

extension ValidationResult {
    var isValid: Bool {
        switch self {
        case .succeed:
            return true
        default:
            return false
        }
    }
}

extension ValidationResult {
    var textColor: UIColor {
        switch self {
        case .initial:
            return UIColor.clear
        case .succeed:
            return UIColor(red: 138.0 / 255.0, green: 221.0 / 255.0, blue: 109.0 / 255.0, alpha: 1.0)
        case .empty:
            return UIColor.red
        case .failed:
            return UIColor.red
        }
    }
}

extension ValidationResult {
    var description: String {
        switch self {
        case .initial:
            return ""
        case let .succeed(message):
            return message
        case let .empty(message):
            return message
        case let .failed(message):
            return message
        }
    }
}
