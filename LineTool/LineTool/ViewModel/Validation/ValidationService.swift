//
//  ValidationService.swift
//  LineTool
//
//  Created by kasim on 2019/2/17.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class ValidationService {
    static let instance = ValidationService()

    private init() {}

    let minCharactersCount = 6

    func validateItemName(itemName: String) -> ValidationResult {
        if itemName.count == 0 {
            return .empty(message: "名稱不可為空")
        }

        return .succeed(message: "")
    }

    func validateItemMoney(itemMoney: String) -> ValidationResult {
        if itemMoney.count == 0 {
            return .empty(message: "金錢不可為空")
        }

        return .succeed(message: "")
    }

    func validateGoodsTitle(goodsTitle: String) -> ValidationResult {
        if goodsTitle.count == 0 {
            return .empty(message: "商品標題不可為空")
        }

        return .succeed(message: "")
    }

    func validateEndDate(endDate: String) -> ValidationResult {
        if endDate.count == 0 {
            return .empty(message: "商品標題不可為空")
        }

        return .succeed(message: "")
    }

    //这里面我们返回一个Observable对象，因为我们这个请求过程需要被监听。
    func validateUsername(_ username: String) -> Observable<ValidationResult> {

        if username.count == 0 {//当字符等于0的时候什么都不做
            return .just(.empty(message: "名稱不可為空"))
        }

        if username.count < minCharactersCount {//当字符小于6的时候返回failed
            return .just(.failed(message: "号码长度至少6个字符"))
        }

        if usernameValid(username) {//检测本地数据库中是否已经存在这个名字
            return .just(.failed(message: "账户已存在"))
        }

        return .just(.succeed(message: "用户名可用"))
    }

    // 从本地数据库中检测用户名是否已经存在
    func usernameValid(_ username: String) -> Bool {
        //        let filePath = NSHomeDirectory() + "/Documents/users.plist"
        //        let userDic = NSDictionary(contentsOfFile: filePath)
        //        let usernameArray = userDic!.allKeys as NSArray
        //        if usernameArray.contains(username) {
        //            return true
        //        } else {
        //            return false
        //        }

        return false
    }

    func validatePassword(_ password: String) -> ValidationResult {
        if password.count == 0 {
            return .empty(message: "密碼不可為空")
        }

        if password.count < minCharactersCount {
            return .failed(message: "密码长度至少6个字符")
        }

        return .succeed(message: "密码可用")
    }

    func validateRepeatedPassword(_ password: String, repeatedPasswordword: String) -> ValidationResult {
        if repeatedPasswordword.count == 0 {
            return .empty(message: "密碼不可為空")
        }

        if repeatedPasswordword == password {
            return .succeed(message: "密码可用")
        }

        return .failed(message: "两次密码不一样")
    }

    func register(_ username: String, password: String) -> Observable<ValidationResult> {
        let userDic = [username: password]

        let filePath = NSHomeDirectory() + "/Documents/users.plist"

        if (userDic as NSDictionary).write(toFile: filePath, atomically: true) {
            return .just(.succeed(message: "注册成功"))
        }
        return .just(.failed(message: "注册失败"))

    }

    func loginUsernameValid(_ username: String) -> Observable<ValidationResult> {
        if username.count == 0 {
            return .just(.empty(message: "名稱不可為空"))
        }

        if usernameValid(username) {
            return .just(.succeed(message: "用户名可用"))
        }
        return .just(.failed(message: "用户名不存在"))
    }

    func login(_ username: String, password: String) -> Observable<ValidationResult> {
        let filePath = NSHomeDirectory() + "/Documents/users.plist"
        let userDic = NSDictionary(contentsOfFile: filePath)
        if let userPass = userDic?.object(forKey: username) as? String {
            if  userPass == password {
                return .just(.succeed(message: "登录成功"))
            }
        }
        return .just(.failed(message: "密码错误"))
    }

}
