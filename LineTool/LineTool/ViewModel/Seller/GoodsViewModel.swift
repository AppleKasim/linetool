//
//  GoodsViewModel.swift
//  LineTool
//
//  Created by kasim on 2019/2/5.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol GoodsType {}

struct GoodsTopGroup: GoodsType {
    var item: BehaviorRelay<GoodsTopItem>

    var titleUsable = BehaviorRelay<ValidationResult>(value: .initial)
    var endDateUsable = BehaviorRelay<ValidationResult>(value: .initial)

    let disposBag = DisposeBag()

    init(buttonTapsMerge: Observable<Void>) {
        item = BehaviorRelay<GoodsTopItem>(value: GoodsTopItem())

        buttonTapsMerge
            .throttle(0.3, latest: true, scheduler: MainScheduler.instance)
            .withLatestFrom(item)
            .map { item -> ValidationResult in
                let validation = ValidationService.instance
                return validation.validateItemName(itemName: item.title)
            }
            .bind(to: titleUsable)
            .disposed(by: disposBag)

        buttonTapsMerge
            .throttle(0.3, latest: true, scheduler: MainScheduler.instance)
            .withLatestFrom(item)
            .map { item -> ValidationResult in
                let validation = ValidationService.instance
                return validation.validateItemMoney(itemMoney: item.endDate)
            }
            .bind(to: endDateUsable)
            .disposed(by: disposBag)
    }

}

struct GoodsItemGroup: GoodsType {
    var item: BehaviorRelay<GoodsItem>
    var nameUsable = BehaviorRelay<ValidationResult>(value: .initial)
    var moneyUsable = BehaviorRelay<ValidationResult>(value: .initial)

    let disposBag = DisposeBag()

    init(buttonTapsMerge: Observable<Void>) {
        item = BehaviorRelay<GoodsItem>(value: GoodsItem())

        buttonTapsMerge
            .throttle(0.3, latest: true, scheduler: MainScheduler.instance)
            .withLatestFrom(item)
            .map { goodsItem -> ValidationResult in
                let validation = ValidationService.instance
                return validation.validateItemName(itemName: goodsItem.name)
        }
        .bind(to: nameUsable)
        .disposed(by: disposBag)

        buttonTapsMerge
            .throttle(0.3, latest: true, scheduler: MainScheduler.instance)
            .withLatestFrom(item)
            .map { goodsItem -> ValidationResult in
                let validation = ValidationService.instance
                return validation.validateItemMoney(itemMoney: goodsItem.money)
        }
        .bind(to: moneyUsable)
        .disposed(by: disposBag)
    }
}

struct GoodsUnderGroup: GoodsType {
    var item: BehaviorRelay<GoodsUnderItem>

    let disposBag = DisposeBag()

    init() {
        item = BehaviorRelay<GoodsUnderItem>(value: GoodsUnderItem())
    }
}

struct GoodesViewModel {

    let disposeBag = DisposeBag()
//    var imageData = BehaviorRelay<[GoodsImageInfo]>(value: [])
    let sections = BehaviorRelay<[GoodsSectionModel]>(value: [])

    //input
    let addTaps = PublishRelay<Void>()
    let deleteItemTaps = PublishRelay<Int>()
    let shelfButtonTaps = PublishRelay<Void>()
    let releaseButtonTaps = PublishRelay<Void>()

    var shelfResult: Driver<Bool> = Observable.just(false).asDriver(onErrorJustReturn: false)
    var releaseResult: Driver<Bool> = Observable.just(false).asDriver(onErrorJustReturn: false)

    init() {
        let buttonTapsMerge = Observable.of(shelfButtonTaps, releaseButtonTaps)
            .merge()

        let initGgoodsTop = GoodsTopGroup(buttonTapsMerge: buttonTapsMerge)
        let initGoodsItem = GoodsItemGroup(buttonTapsMerge: buttonTapsMerge)

        var addImage = GoodsImageInfo()
        addImage.name = "add"

        let goodsImages = [GoodsImageInfo(), GoodsImageInfo(), addImage]
        var imageData = BehaviorRelay<[GoodsImageInfo]>(value: [])
        imageData.accept(goodsImages)


        sections.accept([GoodsSectionModel(header: imageData, items: [initGgoodsTop, initGoodsItem, GoodsUnderGroup()])])

        shelfResult = setButtonTaps(for: shelfButtonTaps)
//            .asObservable()
//            .withLatestFrom(sections) { validation, sections in
//                let imageData = sections[0].header.value.map {
//                    return $0.getImage().jpegData(compressionQuality: 1)!
//                }
//
//                let parameters = sections[0].items.map { item -> [String: Any] in
//                    switch item {
//                    case let top as GoodsTopGroup:
//                        let title = top.item.value.title
//                        let endDate = top.item.value.endDate
//                        return ["title": title, "endDate": endDate]
//                    case let goodsItem as GoodsItemGroup:
//                        let name = goodsItem.item.value.name
//                        let money = goodsItem.item.value.money
//
//                        return ["name": name, "money": money]
//                    case let goodsUnder as GoodsUnderGroup:
//                        let remark = goodsUnder.item.value.remark
//                        return ["remark": remark]
//                    default:
//                        return [:]
//                    }
//                }
//                print(parameters)
//
//
//                //let aaaa = HttpService().test(imageData: imageData, parameters: parameters)
//        }
//            .withLatestFrom(sections) { validation, sections in
//
//
//                validation
//        }

        releaseResult = setButtonTaps(for: releaseButtonTaps)

        addBind()
        deleteBind()
    }

    private func setButtonTaps(for buttonTaps: PublishRelay<Void>) -> Driver<Bool> {
        return buttonTaps
        .throttle(0.3, latest: true, scheduler: MainScheduler.instance)
            .withLatestFrom(sections)
            .map {
                return $0.flatMap { sections in
                    sections.items.compactMap { goods -> (ValidationResult, ValidationResult)? in
                        guard let item = goods as? GoodsItemGroup else {
                            return nil
                        }
                        return (item.nameUsable.value, item.moneyUsable.value)
                    }
                }
            }
            .withLatestFrom(sections) { nameAndMoney, sections -> ((ValidationResult, ValidationResult), [(ValidationResult, ValidationResult)]) in
                let iiii = sections.compactMap { sections -> (ValidationResult, ValidationResult)?in
                    guard let item = sections.items[0] as? GoodsTopGroup else {
                        return nil
                    }
                    return (item.titleUsable.value, item.endDateUsable.value)
                }
                return (iiii[0], nameAndMoney)
            }
            .map { vvv -> Bool in
                if case (.succeed, .succeed) = vvv.0 {
                    let failedQuantity = vvv.1.filter {
                        switch $0 {
                        case (.succeed, .succeed):
                            return false
                        default:
                            return true
                        }
                    }
                    return  failedQuantity.count == 0 ? true : false
                } else {
                    return false
                }
            }.withLatestFrom(sections) { validation, sections in
                let imageData = sections[0].header.value.map {
                    return $0.getImage().jpegData(compressionQuality: 1)!
                }

                let parameters = sections[0].items.map { item -> [String: Any] in
                    switch item {
                    case let top as GoodsTopGroup:
                        let title = top.item.value.title
                        let endDate = top.item.value.endDate
                        return ["title": title, "endDate": endDate]
                    case let goodsItem as GoodsItemGroup:
                        let name = goodsItem.item.value.name
                        let money = goodsItem.item.value.money

                        return ["name": name, "money": money]
                    case let goodsUnder as GoodsUnderGroup:
                        let remark = goodsUnder.item.value.remark
                        return ["remark": remark]
                    default:
                        return [:]
                    }
                }
                print(parameters)

                let pppp = parameters[0]

                var itemPar: [Any] = []
                for index in 1...parameters.count - 2 {
                    itemPar.append(parameters[index])
                }

                let end = parameters[parameters.count - 1]

                return false
                //let aaaa = HttpService().test(imageData: imageData, parameters: parameters)
            }
            .asDriver(onErrorJustReturn: false)
    }

    private func addBind() {
        addTaps.withLatestFrom(sections) { _, sections in

            var items = sections[0].items
            let buttonTapsMerge = Observable.of(self.shelfButtonTaps, self.releaseButtonTaps)
                .merge()

            let item = GoodsItemGroup(buttonTapsMerge: buttonTapsMerge)

            items.insert(item, at: items.count - 1)

            return [SectionModel(header: sections[0].header, items: items)]
            }
            .share(replay: 1, scope: .forever)
            .bind(to: sections)
            .disposed(by: disposeBag)
    }

    private func deleteBind() {
        deleteItemTaps.withLatestFrom(sections) { index, sections in
            var items = sections[0].items
            if items.count != 2 {
                items.remove(at: index)
            }
            return [SectionModel(header: sections[0].header, items: items)]
            }
            .share(replay: 1, scope: .forever)
            .bind(to: sections)
            .disposed(by: disposeBag)
    }

    func generateGoodsItemGroup() -> GoodsItemGroup {
        let buttonTapsMerge = Observable.of(shelfButtonTaps, releaseButtonTaps)
            .merge()

        return GoodsItemGroup(buttonTapsMerge: buttonTapsMerge)
    }

    func updateItem() {
//        var addImage = GoodsImageInfo()
//        addImage.name = "add"
//
//        let goodsImages = [GoodsImageInfo(), GoodsImageInfo(), addImage]
//
//        imageData.accept(goodsImages)
    }

    func addHeaderImage(name: String) {
        var newImage = GoodsImageInfo()
        newImage.name = name

        imageData(insertGoodsImage: newImage)
    }

    func addHeaderImage(image: UIImage) {
        var newImage = GoodsImageInfo()
        newImage.image = image

        imageData(insertGoodsImage: newImage)
    }

    private func imageData(insertGoodsImage: GoodsImageInfo) {
        let imageData = sections.value[0].header
        var goodsImages = imageData.value
        goodsImages.insert(insertGoodsImage, at: goodsImages.count - 1)
        imageData.accept(goodsImages)
    }

    private func test(imageData: [Data], parameters: [String: Any]) -> Observable<Result<[GoodsResponse]>> {
        return HttpService().request(config: SellerApiConfig.goodsUpload(imageData: imageData, urlParameters: parameters))
    }
}
