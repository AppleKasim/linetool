//
//  OrderRecordViewModel.swift
//  LineTool
//
//  Created by kasim on 2019/2/10.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct OrderRecordViewModel {
    var models: Driver<[OrderRecord]>

    init(withSearchText searchText: Observable<String>) {
        models = searchText
            .debug()
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .flatMap { text in
                return SearchService2.shareInstance.getHeros(withName: text)
            }.asDriver(onErrorJustReturn: [])
    }

}

//測試用，之後可能要砍掉
class SearchService2 {
    static let shareInstance = SearchService2()

    private init() {}

    func getHeros() -> Observable<[OrderRecord]> {
        var data: [OrderRecord] = []
        for index in 0...10 {
            var orderRecord = OrderRecord()
            orderRecord.name = "name\(index)"
            orderRecord.status = "status\(index)"
            orderRecord.data = "data\(index)"
            orderRecord.number = "number\(index)"
            orderRecord.allMoney = "allMoney\(index)"
            data.append(orderRecord)
        }

        return Observable.just(data)
            .observeOn(MainScheduler.instance)
    }

    func getHeros(withName name: String) -> Observable<[OrderRecord]> {
        if name == "" {
            return getHeros()
        }

        var data: [OrderRecord] = []
        for index in 0...10 {
            var orderRecord = OrderRecord()
            orderRecord.name = "name\(index)"
            orderRecord.status = "status\(index)"
            orderRecord.data = "data\(index)"
            orderRecord.number = "number\(index)"
            orderRecord.allMoney = "allMoney\(index)"
            data.append(orderRecord)
        }

        data = data.filter {
            $0.name.contains(name)
                || $0.status.contains(name)
                || $0.data.contains(name)
                || $0.number.contains(name)
                || $0.allMoney.contains(name)
        }

        return Observable.just(data)
            .observeOn(MainScheduler.instance)
    }

}
