//
//  HomeViewModel.swift
//  LineTool
//
//  Created by kasim on 2019/2/10.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct HomeViewModel {
    var models: Driver<[Goods]>

    init(withSearchText searchText: Observable<String>) {
        models = searchText
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .flatMap { text in
                return SearchService.instance.getHeros(withName: text)
            }.asDriver(onErrorJustReturn: [])
    }
}

//測試用，之後可能要砍掉
class SearchService {
    static let instance = SearchService()

    private init() {}

    func getHeros() -> Observable<[Goods]> {
        var data: [Goods] = []
        for index in 0...10 {
            var goods = Goods()
            goods.title = "aaaaaaaa\(index)"
            goods.imageNames = ["hhh.jpg"]
            data.append(goods)
        }

        return Observable.just(data)
            .observeOn(MainScheduler.instance)
    }

    func getHeros(withName name: String) -> Observable<[Goods]> {
        if name == "" {
            return getHeros()
        }

        var data: [Goods] = []
        for index in 0...10 {
            var goods = Goods()
            goods.title = "aaaaaaaa\(index)"
            goods.imageNames = ["hhh.jpg"]
            data.append(goods)
        }

        data = data.filter {
            $0.title.contains(name)
        }

        return Observable.just(data)
            .observeOn(MainScheduler.instance)
    }

}
