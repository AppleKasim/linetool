//
//  ChoiceGroupViewModel.swift
//  LineTool
//
//  Created by kasim on 2019/2/10.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class ChoiceGroupViewModel {

    let disposeBag = DisposeBag()
    var groups = BehaviorRelay<[ChoiceGroup]>(value: [])
    var choiceItmes: [BehaviorRelay<Bool>] = []
    var choiceUsable = BehaviorRelay<[BehaviorRelay<Bool>]>(value: [])

    //input
    let sendTaps = PublishSubject<Void>()

    //output
    var sendButtonEnabled = Observable<Bool>.just(false)
//    let sendResult: Observable<Result>

    init() {
        groups.subscribe(onNext: { [weak self] groups in
            guard let strongSelf = self else {
                return
            }
            strongSelf.choiceItmes = groups.map { _ in
                BehaviorRelay<Bool>(value: false)
            }
        }).disposed(by: disposeBag)

        sendButtonEnabled = choiceUsable.asObservable()
            .map {
                for item in $0 where item.value {
                    return true
                }
                return false
            }.distinctUntilChanged()
            .share(replay: 1, scope: .whileConnected)

//        sendResult = sendTaps.asObservable()
//            .flatMapLatest { (username, password) in
//                return service.register(username, password: password)
//                    .observeOn(MainScheduler.instance)
//                    .catchErrorJustReturn(.failed(message: "注册出错"))
//            }
//            .share(replay: 1, scope: .forever)
    }

    func updateItem() {
        var choiceGroups: [ChoiceGroup] = []

        for index in 0...10 {
            var choiceGroup = ChoiceGroup()
            choiceGroup.name = "name\(index)"
            choiceGroups.append(choiceGroup)
        }
        groups.accept(choiceGroups)
    }
}
