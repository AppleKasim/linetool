//
//  ChoiceGoodsViewModel.swift
//  LineTool
//
//  Created by kasim on 2019/2/10.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class ChoiceGoodsViewModel {
    let disposeBag = DisposeBag()
    var goods = BehaviorRelay<[ChoiceGoods]>(value: [])
    var choiceItmes: [BehaviorRelay<Bool>] = []
    var choiceUsable = BehaviorRelay<[BehaviorRelay<Bool>]>(value: [])

    //input
    let sendTaps = PublishSubject<Void>()

    //output
    var sendButtonEnabled = Observable<Bool>.just(false)
    //    let sendResult: Observable<Result>

    init() {
        goods.subscribe(onNext: { [weak self] goods in
            guard let strongSelf = self else {
                return
            }
            strongSelf.choiceItmes = goods.map { _ in
                BehaviorRelay<Bool>(value: false)
            }
        }).disposed(by: disposeBag)

        sendButtonEnabled = choiceUsable.asObservable()
            .map {
                for item in $0 where item.value {
                    return true
                }
                return false
            }.distinctUntilChanged()
            .share(replay: 1, scope: .whileConnected)

        //        sendResult = sendTaps.asObservable()
        //            .flatMapLatest { (username, password) in
        //                return service.register(username, password: password)
        //                    .observeOn(MainScheduler.instance)
        //                    .catchErrorJustReturn(.failed(message: "注册出错"))
        //            }
        //            .share(replay: 1, scope: .forever)
    }

    func updateItem() {
        var choiceGoodsArray: [ChoiceGoods] = []

        for index in 0...10 {
            var choiceGoods = ChoiceGoods()
            choiceGoods.goodsImageUrl = "hhh.jpg"
            choiceGoods.title = "title\(index)"
            choiceGoods.content = "content\(index)"
            choiceGoodsArray.append(choiceGoods)
        }
        goods.accept(choiceGoodsArray)
    }

}
