//
//  Reactive.swift
//  LineTool
//
//  Created by kasim on 2019/2/14.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base: UILabel {
    var validationResult: Binder<ValidationResult> {
        return Binder.init(base, binding: { (label, result) in
            label.textColor = result.textColor
            label.text = result.description
        })
    }
}

extension Reactive where Base: UITextField {
    var inputEnabled: Binder<ValidationResult> {      //這裡的Bool，'應該'是沒有意義的
        return Binder(self.base) { textFiled, result in
            textFiled.isEnabled = result.isValid
        }
    }
}

extension Reactive where Base: UIBarButtonItem {
    var tapEnabled: Binder<ValidationResult> {      //這裡的Bool，'應該'是沒有意義的
        return Binder(self.base) { button, result in
            button.isEnabled = result.isValid
        }
    }
}
