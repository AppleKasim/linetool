//
//  ChoiceGoodsViewController.swift
//  LineTool
//
//  Created by kasim on 2019/2/9.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ChoiceGoodsViewController: UIViewController {

    let viewModel = ChoiceGoodsViewModel()
    let disposeBag = DisposeBag()

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.updateItem()

        tableView.rx.itemSelected
            .subscribe(onNext: {[weak self] indexPath in
                guard let strongSelf = self else { return }
                let new = !strongSelf.viewModel.choiceItmes[indexPath.row].value
                strongSelf.viewModel.choiceItmes[indexPath.row].accept(new)
                strongSelf.viewModel.choiceUsable.accept(strongSelf.viewModel.choiceItmes)
            }).disposed(by: disposeBag)

        // swiftlint:disable line_length
        viewModel.goods.asDriver()
            .drive(tableView.rx.items(cellIdentifier: "ChoiceGoodsCell", cellType: ChoiceGoodsCell.self)) { [weak self] (row, element, cell) in

                cell.update(model: element)

                guard let strongSelf = self,
                    strongSelf.viewModel.choiceItmes.count > row else {
                        return
                }
                cell.subscribeChoiceImage(relay: strongSelf.viewModel.choiceItmes[row])
            }
            .disposed(by: disposeBag)
        // swiftlint:enable line_length
    }
}

extension ChoiceGoodsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        // swiftlint:disable line_length
        guard let footer = tableView.dequeueReusableCell(withIdentifier: "ChoiceGoodsFooter") as? ChoiceGoodsFooter else {
            return nil
        }
        // swiftlint:enable line_length
        footer.sendButton.setTitle("確認", for: .normal)

        viewModel.sendButtonEnabled
            .subscribe(onNext: { valid in
                footer.sendButton.isEnabled = valid
                footer.sendButton.alpha = valid ? 1.0 : 0.5
            })
            .disposed(by: footer.disposeBag)

        footer.sendButton.rx.tap
            .debounce(2, scheduler: MainScheduler.instance)
            .bind(to: viewModel.sendTaps)
            .disposed(by: disposeBag)

        //        viewModel.sendResult
        //            .subscribe(onNext: { [unowned self] result in
        //                switch result {
        //                case let .ok(message):
        //                    print("OK\(message)")
        ////                    self.showAlert(message: message)
        //                case .empty:
        //                    print("empty")
        ////                    self.showAlert(message: "")
        //                case let .failed(message):
        //                    print("failed\(message)")
        ////                    self.showAlert(message: message)
        //                }
        //            })
        //            .disposed(by: disposeBag)

        return footer
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0001
    }
}
