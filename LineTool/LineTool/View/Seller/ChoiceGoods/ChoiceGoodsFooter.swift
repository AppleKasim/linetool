//
//  SendButtonFooter.swift
//  LineTool
//
//  Created by kasim on 2019/2/11.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ChoiceGoodsFooter: UITableViewCell {

    let disposeBag = DisposeBag()

    @IBOutlet weak var sendButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
