//
//  ChoiceGroupViewController.swift
//  LineTool
//
//  Created by kasim on 2019/2/9.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ChoiceGroupViewController: UIViewController {

    let viewModel = ChoiceGroupViewModel()
    let disposeBag = DisposeBag()

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.updateItem()

        tableView.rx.itemSelected
            .subscribe(onNext: {[weak self] indexPath in
                guard let strongSelf = self else { return }
                let new = !strongSelf.viewModel.choiceItmes[indexPath.row].value
                strongSelf.viewModel.choiceItmes[indexPath.row].accept(new)
                strongSelf.viewModel.choiceUsable.accept(strongSelf.viewModel.choiceItmes)
            }).disposed(by: disposeBag)

        // swiftlint:disable line_length
        viewModel.groups.asDriver()
            .drive(tableView.rx.items(cellIdentifier: "ChoiceGroupCell", cellType: ChoiceGroupCell.self)) { [weak self] (row, element, cell) in

                cell.update(model: element)

                guard let strongSelf = self,
                    strongSelf.viewModel.choiceItmes.count > row else {
                    return
                }
                cell.subscribeChoiceImage(relay: strongSelf.viewModel.choiceItmes[row])
            }
            .disposed(by: disposeBag)
        // swiftlint:enable line_length
    }
}

extension ChoiceGroupViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        // swiftlint:disable line_length
        guard let footer = tableView.dequeueReusableCell(withIdentifier: "ChoiceGroupFooter") as? ChoiceGroupFooter else {
            return nil
        }
        // swiftlint:enable line_length

        footer.sendButton.setTitle("確認", for: .normal)

        viewModel.sendButtonEnabled
            .subscribe(onNext: { valid in
                footer.sendButton.isEnabled = valid
                footer.sendButton.alpha = valid ? 1.0 : 0.5
            })
            .disposed(by: footer.disposeBag)

        footer.sendButton.rx.tap
            .bind(to: viewModel.sendTaps)
            .disposed(by: disposeBag)

//        viewModel.sendResult
//            .subscribe(onNext: { [unowned self] result in
//                switch result {
//                case let .ok(message):
//                    print("OK\(message)")
////                    self.showAlert(message: message)
//                case .empty:
//                    print("empty")
////                    self.showAlert(message: "")
//                case let .failed(message):
//                    print("failed\(message)")
////                    self.showAlert(message: message)
//                }
//            })
//            .disposed(by: disposeBag)

        return footer
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0001
    }
}
