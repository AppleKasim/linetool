//
//  ChoiceGroupCell.swift
//  LineTool
//
//  Created by kasim on 2019/2/10.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ChoiceGroupCell: UITableViewCell {

    var disposeBag = DisposeBag()

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var choiceImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() //復用cell時，移除disposeBag。避免顯示錯誤
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: ChoiceGroup) {
        nameLabel.text = model.name
    }

    func subscribeChoiceImage(relay: BehaviorRelay<Bool>) {
        relay.subscribe(onNext: { [weak self] choice in
            guard let strongSelf = self else {
                return
            }
            if choice {
                strongSelf.choiceImageView?.image = UIImage(named: "Select")
            } else {
                strongSelf.choiceImageView?.image = UIImage(named: "NoSelect")
            }
        }).disposed(by: disposeBag)
    }

}
