//
//  OrderRecordViewController.swift
//  LineTool
//
//  Created by kasim on 2019/2/3.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class OrderRecordViewController: UIViewController {

    var searchBarText: Observable<String> {
        return searchBar.rx.text.orEmpty
            .throttle(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
    }

    let disposeBag = DisposeBag()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()

        let viewModel = OrderRecordViewModel(withSearchText: searchBarText)

        // swiftlint:disable line_length
        viewModel.models
            .drive(tableView.rx.items(cellIdentifier: "OrderRecordCell", cellType: OrderRecordCell.self)) {(_, element, cell) in
                cell.update(model: element)
            }
            .disposed(by: disposeBag)
        // swiftlint:enable line_length
    }

}

extension OrderRecordViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}
