//
//  OrderRecordCell.swift
//  LineTool
//
//  Created by kasim on 2019/2/10.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit

class OrderRecordCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var allMoneyLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: OrderRecord) {
        nameLabel.text = model.name
        dateLabel.text = model.data
        statusLabel.text = model.status
        numberLabel.text = model.number
        allMoneyLabel.text = model.allMoney
    }

}
