//
//  PhotoLoopViewController.swift
//  LineTool
//
//  Created by kasim on 2019/2/7.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PhotoLoopViewController: UIViewController {

    var viewModel = PhotoLoopViewModel()
    let disposeBag = DisposeBag()
    var imageEditor: CLImageEditor!

    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }

    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var pageLabel: UILabel!

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let newX = CGFloat(viewModel.currentIndex.value) * scrollView.frame.width
        scrollView.setContentOffset(CGPoint(x: newX, y: 0), animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.imageData.subscribe(onNext: {[weak self] imageName in
            guard let strongSelf = self else { return }

            strongSelf.viewModel.photoViews = imageName.compactMap {
                let nib = Bundle.main.loadNibNamed("PhotoLoopView", owner: self, options: nil)?.first
                guard let photoView = nib as? PhotoLoopView else {
                    return nil
                }

                photoView.imageView.image = $0.getImage()
                return photoView
            }

            guard let scrollView = strongSelf.scrollView else {
                return
            }

            scrollView.isPagingEnabled = true

            for index in strongSelf.viewModel.photoViews.indices {
                let view = strongSelf.viewModel.photoViews[index]
                view.translatesAutoresizingMaskIntoConstraints = false
                scrollView.addSubview(strongSelf.viewModel.photoViews[index])

                if index == 0 {
                    view.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
                } else {
                    let previousView = strongSelf.viewModel.photoViews[index - 1]
                    view.leadingAnchor.constraint(equalTo: previousView.trailingAnchor).isActive = true
                }
                if index == strongSelf.viewModel.photoViews.count - 1 {
                    view.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
                }
                view.centerYAnchor.constraint(equalTo: scrollView.centerYAnchor).isActive = true
                view.heightAnchor.constraint(equalTo: scrollView.heightAnchor).isActive = true
                view.widthAnchor.constraint(equalTo: strongSelf.view.widthAnchor).isActive = true
            }
        })
        .disposed(by: disposeBag)

        setCurrentIndexDrive()
    }

    func setCurrentIndexDrive() {
        viewModel.currentIndex.asDriver()
            .drive(onNext: { [weak self] index in
                guard let strongSelf = self else {
                    return
                }

                if index == 0 {
                    strongSelf.previousButton.isEnabled = false
                } else {
                    strongSelf.previousButton.isEnabled = true
                }

                if index == strongSelf.viewModel.imageData.value.count - 1 {
                    strongSelf.nextButton.isEnabled = false
                } else {
                    strongSelf.nextButton.isEnabled = true
                }

                let newX = CGFloat(index) * strongSelf.scrollView.frame.width
                strongSelf.scrollView.setContentOffset(CGPoint(x: newX, y: 0), animated: true)
                strongSelf.pageLabel.text = "\(index + 1)/\(strongSelf.viewModel.imageData.value.count)"
            })
            .disposed(by: disposeBag)
    }

    @IBAction func goForward(_ sender: UIButton) {
        let index = viewModel.currentIndex.value - 1
        viewModel.currentIndex.accept(index)
    }

    @IBAction func later(_ sender: UIButton) {
        let index = viewModel.currentIndex.value + 1
        viewModel.currentIndex.accept(index)
    }

    @IBAction func edit(_ sender: UIBarButtonItem) {
        let currentImage = viewModel.imageData.value[viewModel.currentIndex.value].getImage()
        imageEditor = CLImageEditor(image: currentImage)
        imageEditor.delegate = self

        present(imageEditor, animated: true, completion: nil)
    }
}

extension PhotoLoopViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x / view.frame.width)
        if Int(pageIndex) != viewModel.currentIndex.value {
            viewModel.currentIndex.accept(Int(pageIndex))
        }
    }
}

extension PhotoLoopViewController: CLImageEditorDelegate, CLImageEditorThemeDelegate, CLImageEditorTransitionDelegate {

    func imageEditor(_ editor: CLImageEditor!, didFinishEditingWith image: UIImage!) {
//        let imageData = viewModel.imageData.map { [weak self] imageNames in
//            guard let strongSelf = self else {
//                return
//            }
//            $0[]
//        }
//        imageView.image = image

        imageEditor.dismiss(animated: true, completion: nil)
    }

    func imageEditor(_ editor: CLImageEditor!, didDismissWith imageView: UIImageView!, canceled: Bool) {

    }
}
