//
//  GoodsViewController.swift
//  LineTool
//
//  Created by kasim on 2019/2/3.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

typealias GoodsSectionModel = SectionModel<BehaviorRelay<[GoodsImageInfo]>, GoodsType>

class GoodsViewController: UIViewController, UINavigationControllerDelegate {

    var viewModel = GoodesViewModel()
    var imageEditor: CLImageEditor!

    let disposeBag = DisposeBag()

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            let headerNib = UINib.init(nibName: "GoodsHeader", bundle: Bundle.main)
            tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "GoodsHeader")

            tableView.rx.itemSelected
                .subscribe(onNext: {[weak self] _ in
                    guard let strongSelf = self else { return }
                    strongSelf.view.endEditing(true)
                }).disposed(by: disposeBag)
        }
    }

    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<GoodsSectionModel>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<GoodsSectionModel>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, element) in

        guard let strongSelf = self else {
            return UITableViewCell()
        }

        switch element {
        case let goodsTopGroup as GoodsTopGroup:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTopCell") as? ItemTopCell else {
                return UITableViewCell()
            }

            var topItem = goodsTopGroup.item.value

            cell.titleTextField.rx.controlEvent([.editingDidEnd])
                .asObservable()
                .withLatestFrom(cell.titleTextField.rx.text.orEmpty)
                .subscribe(onNext: { title in
                    topItem.title = title
                    goodsTopGroup.item.accept(topItem)
                })
                .disposed(by: cell.disposeBag)

            cell.endDateTextField.rx.controlEvent([.editingDidEnd])
                .asObservable()
                .withLatestFrom(cell.endDateTextField.rx.text.orEmpty)
                .subscribe(onNext: { endDate in
                    topItem.endDate = endDate
                    goodsTopGroup.item.accept(topItem)
                }).disposed(by: cell.disposeBag)

            goodsTopGroup.titleUsable
                .asDriver(onErrorJustReturn: .failed(message: "錯誤"))
                .drive(
                    cell.titleVlidationLabel.rx.validationResult
                )
                .disposed(by: cell.disposeBag)

            goodsTopGroup.endDateUsable
                .asDriver(onErrorJustReturn: .failed(message: "錯誤"))
                .drive(
                    cell.endDateVlidationLabel.rx.validationResult
                )
                .disposed(by: cell.disposeBag)

            cell.update(item: topItem)

            return cell

        case let goodsItem as GoodsItemGroup:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ItemsCell") as? ItemCell else {
                return UITableViewCell()
            }

            var currentGoodsItem = goodsItem.item.value

            cell.nameTextField.rx.controlEvent([.editingDidEnd])
                .asObservable()
                .withLatestFrom(cell.nameTextField.rx.text.orEmpty)
                .subscribe(onNext: { name in
                    currentGoodsItem.name = name
                    goodsItem.item.accept(currentGoodsItem)
                })
                .disposed(by: cell.disposeBag)

            cell.moneyTextField.rx.controlEvent([.editingDidEnd])
                .asObservable()
                .withLatestFrom(cell.moneyTextField.rx.text.orEmpty)
                .subscribe(onNext: { money in
                    currentGoodsItem.money = money
                    goodsItem.item.accept(currentGoodsItem)
                }).disposed(by: cell.disposeBag)

            goodsItem.nameUsable
                .asDriver(onErrorJustReturn: .failed(message: "錯誤"))
                .drive(
                    cell.nameVlidationLabel.rx.validationResult
                )
                .disposed(by: cell.disposeBag)

            goodsItem.moneyUsable
                .asDriver(onErrorJustReturn: .failed(message: "錯誤"))
                .drive(
                    cell.moneyValidationLabel.rx.validationResult
                )
                .disposed(by: cell.disposeBag)

            cell.deleteButton.rx.tap
                .map { return indexPath.row }
                .bind(to: strongSelf.viewModel.deleteItemTaps)
                .disposed(by: cell.disposeBag)

            cell.update(item: currentGoodsItem)

            return cell

        case let goodsUnder as GoodsUnderGroup:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "UnderCell") as? UnderCell else {
                return UITableViewCell()
            }

            cell.addButton.rx.tap
                .bind(to: strongSelf.viewModel.addTaps)
                .disposed(by: cell.disposeBag)

            return cell
        default:
            return UITableViewCell()
        }
    }
    // swiftlint:enable line_length

    override func viewDidLoad() {
        super.viewDidLoad()

        //需要再viewModel.items.bind之前，不然使用tableView style grouped，上方會出現空白。
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        viewModel.sections
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        viewModel.updateItem()
    }
}

extension GoodsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let nib = Bundle.main.loadNibNamed("GoodsHeader", owner: self, options: nil)?.last
        guard let headerCell = nib as? GoodsHeader else {
            return UIView()
        }

        dataSource.sectionModels[section].header
            .bind(to: headerCell.imageData)
            .disposed(by: disposeBag)

        headerCell.collectionView.rx
            .itemSelected
            .subscribe(onNext: {[weak self] indexPath in
                guard let strongSelf = self else { return }
                if headerCell.isAddItem(index: indexPath.row) {
                    headerCell.setAddImageAlert(viewController: strongSelf)
                } else {
                    guard let navigationController = strongSelf.navigationController else { return }
                    headerCell.pushPhotoLoopPage(navigationController: navigationController, index: indexPath.row)
                }
            }).disposed(by: headerCell.disposeBag)

        return headerCell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == dataSource[0].items.count - 1 {
            return 200
        }
        return 150
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let footer = tableView.dequeueReusableCell(withIdentifier: "GoodsFooter") as? GoodsFooter else {
            return nil
        }

        footer.shelfButton.rx.tap
            .bind(to: viewModel.shelfButtonTaps)
            .disposed(by: footer.disposeBag)

        viewModel.shelfResult
            .drive(onNext: { [unowned self] approve in
                if approve {
                    self.pushViewController(withIdentifier: "ChoiceGoodsViewController")
                }
            })
        .disposed(by: footer.disposeBag)

        footer.releaseButton.rx.tap
            .bind(to: viewModel.releaseButtonTaps)
            .disposed(by: footer.disposeBag)

        viewModel.releaseResult
            .drive(onNext: { [unowned self] approve in
                if approve {
                    self.pushViewController(withIdentifier: "ChoiceGroupViewController")
                }
            })
            .disposed(by: footer.disposeBag)

        return footer
    }

    func pushViewController(withIdentifier: String) {
        let storyboard = UIStoryboard(name: "Seller", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: withIdentifier)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension GoodsViewController: UIImagePickerControllerDelegate {
    // swiftlint:disable line_length
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let image = info[.originalImage] as? UIImage else {
            return
        }
        //要到編輯圖片頁面
        imageEditor = CLImageEditor(image: image)
        imageEditor.delegate = self

        picker.present(imageEditor, animated: true, completion: nil)
    }
    // swiftlint:enable line_length
}

extension GoodsViewController: CLImageEditorDelegate, CLImageEditorThemeDelegate, CLImageEditorTransitionDelegate {

    func imageEditor(_ editor: CLImageEditor!, didFinishEditingWith image: UIImage!) {
        viewModel.addHeaderImage(image: image)
        imageEditor.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
    }

    func imageEditor(_ editor: CLImageEditor!, didDismissWith imageView: UIImageView!, canceled: Bool) {

    }
}
