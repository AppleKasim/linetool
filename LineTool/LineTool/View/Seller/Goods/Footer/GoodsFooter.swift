//
//  GoodsFooter.swift
//  LineTool
//
//  Created by kasim on 2019/2/9.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class GoodsFooter: UITableViewCell {

    let disposeBag = DisposeBag()

    @IBOutlet weak var releaseButton: UIButton!
    @IBOutlet weak var shelfButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
