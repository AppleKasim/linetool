//
//  GoodsHeader.swift
//  LineTool
//
//  Created by kasim on 2019/2/6.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GoodsHeader: UITableViewCell {

    //var goodsViewModel = GoodesViewModel()
    var imageData = BehaviorRelay<[GoodsImageInfo]>(value: [])
    let disposeBag = DisposeBag()

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            // swiftlint:disable line_length
            self.collectionView!.register(UINib(nibName: "GoodsHeaderImageCell", bundle: nil), forCellWithReuseIdentifier: "GoodsHeaderImageCell")
            // swiftlint:enable line_length
        }
    }

    @IBOutlet weak var collectionViewLayout: UICollectionViewFlowLayout! {
        didSet {
            let height = 100 / 667 * Global.screenHeight
            let width = height
            collectionViewLayout.itemSize = CGSize(width: width, height: height)
            collectionViewLayout.minimumLineSpacing = 6 / 375 * Global.screenWidth
            collectionViewLayout.minimumInteritemSpacing = 5 / 667 * Global.screenHeight
            let edgeInsets = 25 / 375 * Global.screenWidth
            collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: edgeInsets, bottom: 0, right: edgeInsets)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        imageData.asObservable()
            .bind(to: collectionView.rx.items) { (collectionView, row, element) in
                let indexPath = IndexPath(row: row, section: 0)
                let cellIdentifier = "GoodsHeaderImageCell"
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
                guard let header = cell as? GoodsHeaderImageCell else {
                    return UICollectionViewCell()
                }

                header.update(goodsImage: element)

                return header
            }
            .disposed(by: disposeBag)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func isAddItem(index: Int) -> Bool {
        return imageData.value.count - 1 == index ? true: false
    }

    func setAddImageAlert(viewController: GoodsViewController) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = viewController
        UIAlertController.present(
            in: viewController,
            title: nil,
            message: nil,
            style: UIDevice.current.userInterfaceIdiom == .phone ? .actionSheet : .alert,
            actions: [
                .init(title: "照相", style: .default) {[unowned viewController] _ in
                    imagePicker.sourceType = .camera
                    viewController.present(imagePicker, animated: true, completion: nil)
                },
                .init(title: "相簿", style: .default) {[unowned viewController] _ in
                    imagePicker.sourceType = .photoLibrary
                    viewController.present(imagePicker, animated: true, completion: nil)
                },
                .init(title: "cancel", style: .cancel) { _ in
                }
            ])
    }

    func pushPhotoLoopPage(navigationController: UINavigationController, index: Int) {
        let storyboard = UIStoryboard(name: "Seller", bundle: nil)
        let instantiation = storyboard.instantiateViewController(withIdentifier: "PhotoLoopViewController")
        guard let viewController = instantiation as? PhotoLoopViewController else {
            return
        }

        imageData
            .map({ imageNames -> [GoodsImageInfo] in
                var new = imageNames
                new.removeLast()
                return new
            })
            .bind(to: viewController.viewModel.imageData)
            .disposed(by: disposeBag)

        viewController.viewModel.currentIndex.accept(index)

        navigationController.pushViewController(viewController, animated: true)
    }

}
