//
//  GoodsHeaderImageCell.swift
//  LineTool
//
//  Created by kasim on 2019/2/5.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit

class GoodsHeaderImageCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func update(goodsImage: GoodsImageInfo) {
        imageView.image = goodsImage.getImage()
    }

}
