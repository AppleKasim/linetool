//
//  RemarkCell.swift
//  LineTool
//
//  Created by kasim on 2019/2/11.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class UnderCell: UITableViewCell {

    var disposeBag = DisposeBag()

    @IBOutlet weak var textView: UITextView! {
        didSet {
            textView.layer.borderWidth = 1
        }
    }

    @IBOutlet weak var addButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() //復用cell時，移除disposeBag。避免顯示錯誤
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
