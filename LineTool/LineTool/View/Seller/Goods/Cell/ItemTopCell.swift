//
//  ItemTopCell.swift
//  LineTool
//
//  Created by kasim on 2019/2/28.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ItemTopCell: UITableViewCell {

    let disposeBag = DisposeBag()

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var titleVlidationLabel: UILabel! {
        didSet {
            titleVlidationLabel.text = ""
        }
    }

    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var endDateVlidationLabel: UILabel! {
        didSet {
            endDateVlidationLabel.text = ""
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(item: GoodsTopItem) {
        titleTextField.text = item.title
        endDateTextField.text = item.endDate
    }

}
