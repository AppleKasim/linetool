//
//  ItemCell.swift
//  LineTool
//
//  Created by kasim on 2019/2/5.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ItemCell: UITableViewCell {

    var disposeBag = DisposeBag()

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nameVlidationLabel: UILabel! {
        didSet {
            nameVlidationLabel.text = ""
        }
    }

    @IBOutlet weak var moneyTextField: UITextField!
    @IBOutlet weak var moneyValidationLabel: UILabel! {
        didSet {
            moneyValidationLabel.text = ""
        }
    }

    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var deleteButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() //復用cell時，移除disposeBag。避免顯示錯誤
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func update(item: GoodsItem) {
        nameTextField.text = item.name
        moneyTextField.text = item.money
        numberTextField.text = ""
    }

}
