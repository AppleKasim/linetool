//
//  HomeTableViewController.swift
//  LineTool
//
//  Created by kasim on 2019/3/5.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class HomeTableViewController: UITableViewController {

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.dataSource = nil

        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search name..."
        searchController.searchBar.autocapitalizationType = .none
        navigationItem.searchController = searchController

        let searchBarText = searchController.searchBar.rx.text.orEmpty
                .throttle(0.3, scheduler: MainScheduler.instance)
                .distinctUntilChanged()

        let viewModel = HomeViewModel(withSearchText: searchBarText)

        viewModel.models
            .drive(tableView.rx.items(cellIdentifier: "HomeCell", cellType: HomeCell.self)) {(_, element, cell) in
                cell.update(model: element)
            }
            .disposed(by: disposeBag)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
}

