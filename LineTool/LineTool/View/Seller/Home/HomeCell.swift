//
//  HomeCell.swift
//  LineTool
//
//  Created by kasim on 2019/2/10.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {

    @IBOutlet weak var goodsImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: Goods) {
        goodsImageView.image = UIImage(named: model.imageNames[0])
        titleLabel.text = model.title
    }

}
