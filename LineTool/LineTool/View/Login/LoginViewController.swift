//
//  LoginViewController.swift
//  LineTool
//
//  Created by kasim on 2019/2/3.
//  Copyright © 2019 kasim. All rights reserved.
//

import UIKit
import LineSDK

class LoginViewController: UIViewController, IndicatorDisplay {

    override func viewDidLoad() {
        super.viewDidLoad()

        let loginButton = LoginButton()

        loginButton.delegate = self
        loginButton.presentingViewController = self

        // You could set the permissions you need or use default permissions
        loginButton.permissions = [.profile, .friends, .groups, .messageWrite, .openID]

        view.addSubview(loginButton)

        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}

extension LoginViewController: LoginButtonDelegate {
    func loginButton(_ button: LoginButton, didSucceedLogin loginResult: LoginResult) {
        hideIndicator()
        print("Login Successd.")
        UIAlertController.present(in: self, successResult: "\(loginResult)") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let viewController = storyboard
                .instantiateViewController(withIdentifier: "CustomTabBarController") as? CustomTabBarController,
                let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                    return
            }
            appDelegate.window!.rootViewController = viewController
        }
    }

    func loginButton(_ button: LoginButton, didFailLogin error: Error) {
        hideIndicator()
        print("Error: \(error)")
    }

    func loginButtonDidStartLogin(_ button: LoginButton) {
        showIndicator()
        print("Login Started.")
    }
}
