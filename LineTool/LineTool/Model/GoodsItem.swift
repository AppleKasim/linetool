//
//  GoodsItemMoney.swift
//  LineTool
//
//  Created by kasim on 2019/2/9.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation

struct Goods {
    var title: String = ""
    var imageNames: [String] = []
    var item: [GoodsItem] = []
}

struct GoodsResponse: Codable {
    var test: String = ""
}

struct GoodsTopItem {
    var title: String = ""
    var endDate: String = ""
}

struct GoodsItem {
    var name: String = ""
    var money: String = ""
}

struct GoodsUnderItem {
    var remark: String = ""
}
