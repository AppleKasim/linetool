//
//  ChoiceGoods.swift
//  LineTool
//
//  Created by kasim on 2019/2/10.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct ChoiceGoods {
    var goodsImageUrl: String = ""
    var title: String = ""
    var content: String = ""
    var isChoice: Bool = false
}
