//
//  ImageName.swift
//  LineTool
//
//  Created by kasim on 2019/2/5.
//  Copyright © 2019 kasim. All rights reserved.
//

struct GoodsImageInfo {
    var name: String = ""
    var image: UIImage?

    func getImage() -> UIImage {
        if name != "", let nameImage = UIImage(named: name) {
            return nameImage
        } else if let strongImage = image {
            return strongImage
        } else if let defaultImage = UIImage(named: "hhh.jpg") {
            return defaultImage
        } else {
            return UIImage()
        }
    }
}
