//
//  SectionModel.swift
//  LineTool
//
//  Created by kasim on 2019/2/27.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
import RxDataSources

struct SectionModel<Header, T>: SectionModelType {

    typealias Item = T

    var header: Header
    var items: [T]

    init(header: Header, items: [T]) {
        self.header = header
        self.items = items
    }

    init(original: SectionModel<Header, T>, items: [T]) {
        self = original
        self.items = items
    }
}
