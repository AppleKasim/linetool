//
//  OrderRecord.swift
//  LineTool
//
//  Created by kasim on 2019/2/10.
//  Copyright © 2019 kasim. All rights reserved.
//

import Foundation
struct OrderRecord {
    var name: String = ""
    var status: String = ""
    var data: String = ""
    var number: String = ""
    var allMoney: String = ""
}
