//
//  GlobalConstant.swift
//  Voice
//
//  Created by kasim on 2018/10/26.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import AVFoundation
import MediaPlayer

struct Global {
    private init() {

    }

    static let baseUrl = "http://linebuy.walkear.com"

    //Size
    static let screenWidth = UIScreen.main.bounds.width
    static let screenHeight = UIScreen.main.bounds.height
}
